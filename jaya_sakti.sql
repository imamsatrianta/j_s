/*
 Navicat Premium Data Transfer

 Source Server         : JEM
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : jaya_sakti

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 02/11/2018 23:44:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for data
-- ----------------------------
DROP TABLE IF EXISTS `data`;
CREATE TABLE `data`  (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `stok` int(11) NOT NULL,
  PRIMARY KEY (`id_barang`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data
-- ----------------------------
INSERT INTO `data` VALUES (1, 'Rinso Cair', 1);
INSERT INTO `data` VALUES (2, 'Sikat Gigi Formula', 2);
INSERT INTO `data` VALUES (3, 'Pepsodent', 0);

-- ----------------------------
-- Table structure for mu_agama
-- ----------------------------
DROP TABLE IF EXISTS `mu_agama`;
CREATE TABLE `mu_agama`  (
  `id_agama` int(11) NOT NULL AUTO_INCREMENT,
  `nama_agama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_agama`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_agama
-- ----------------------------
INSERT INTO `mu_agama` VALUES (1, 'Islam');
INSERT INTO `mu_agama` VALUES (2, 'Katholik');
INSERT INTO `mu_agama` VALUES (3, 'Protestan');
INSERT INTO `mu_agama` VALUES (4, 'Hindu');
INSERT INTO `mu_agama` VALUES (5, 'Budha');
INSERT INTO `mu_agama` VALUES (6, 'Konghucu');
INSERT INTO `mu_agama` VALUES (7, 'Kepercayaan');

-- ----------------------------
-- Table structure for mu_bahasa
-- ----------------------------
DROP TABLE IF EXISTS `mu_bahasa`;
CREATE TABLE `mu_bahasa`  (
  `id_bahasa` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bahasa` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_bahasa`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_bahasa
-- ----------------------------
INSERT INTO `mu_bahasa` VALUES (1, 'Bahasa Indonesia', 1);
INSERT INTO `mu_bahasa` VALUES (2, 'Bahasa Inggris', 1);
INSERT INTO `mu_bahasa` VALUES (3, 'Bahasa mandarin', 1);

-- ----------------------------
-- Table structure for mu_barang
-- ----------------------------
DROP TABLE IF EXISTS `mu_barang`;
CREATE TABLE `mu_barang`  (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `merek_brand` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `model_type` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `berat_bruto` float NOT NULL,
  `ukuran_volume` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `warna` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_subkategori` int(11) NOT NULL,
  `id_rak` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `jml_minimal` int(11) NOT NULL,
  `jml_maksimal` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `ppn` int(11) NOT NULL DEFAULT 10,
  `kode_satuan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan_barang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status_jual` enum('ya','tidak') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_input` datetime(0) NOT NULL,
  PRIMARY KEY (`id_barang`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_barang
-- ----------------------------
INSERT INTO `mu_barang` VALUES (1, 'BRG-000001', 'Filter Oli Land Cruiser VX80', '-', 'Cair', 1000, '10cm x 10 cm', 'putih', 1, 0, 1, 150000, 1, 200, 7, 10, 'lsn', 'Tuliskan Keterangan Disini,..', 'Filter_oli_land_Cruiser_VX80_.jpg', 'ya', 1, '2018-10-12 11:21:17');
INSERT INTO `mu_barang` VALUES (2, 'BRG-000002', 'Shell Helix', 'My Baby', '-', 0.2, '-', 'Pink', 2, 1, 2, 35000, 10, 100, 8, 10, 'bh', 'Barang ini hanya boleh dijual 1 perharinya. ', 'pigeon.jpg', 'ya', 1, '2018-10-26 16:43:55');
INSERT INTO `mu_barang` VALUES (3, 'BRG-000003', 'Spion Honda 125', 'Oishi', '-', 0.1, '-', 'kuning', 2, 1, 1, 1500, 12, 78, 9, 10, 'bh', '', '', 'ya', 1, '2018-10-26 16:44:18');
INSERT INTO `mu_barang` VALUES (4, 'BRG-000004', 'Rantai Keteng', 'Indofood', 'Instant', 0, '15cm x 10 cm', 'kuning', 1, 0, 2, 2300, 10, 120, 4, 10, 'bh', 'Tidak Ada Keterangan,..', '', 'ya', 1, '2018-10-26 16:44:32');
INSERT INTO `mu_barang` VALUES (5, 'BRG-000005', 'Minyak Rem', '', '', 5500, '', '', 2, 1, 1, 10000, 1, 100, 30, 10, 'bh', '', '', 'ya', 1, '2018-11-02 14:52:19');
INSERT INTO `mu_barang` VALUES (6, 'BRG-000006', 'Oli MPX', 'MPX', '', 1000, '1', '', 2, 1, 3, 33000, 1, 100, 10, 10, 'bh', 'Oli', '', 'ya', 1, '2018-11-02 15:15:20');
INSERT INTO `mu_barang` VALUES (7, 'BRG-000007', 'Oli Samping', 'Shell', 'Shell', 1000, '10cm', 'Kuning', 2, 1, 2, 22000, 1, 100, 15, 10, 'bh', '', '', 'ya', 1, '2018-11-02 17:30:22');
INSERT INTO `mu_barang` VALUES (8, 'BRG-000008', 'Velg', 'IRC', 'IRC', 5000, '', '', 1, 0, 0, 150000, 1, 12, 0, 10, 'bh', 'Velg', '', 'ya', 1, '2018-11-02 17:44:07');

-- ----------------------------
-- Table structure for mu_barang_harga
-- ----------------------------
DROP TABLE IF EXISTS `mu_barang_harga`;
CREATE TABLE `mu_barang_harga`  (
  `id_barang_harga` int(11) NOT NULL AUTO_INCREMENT,
  `id_barang` int(11) NOT NULL,
  `id_kategori_pelanggan` int(11) NOT NULL,
  `id_jenis_jual` int(11) NOT NULL,
  `harga` varchar(111) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `persen_beli` varchar(111) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diskon` varchar(111) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah` varchar(111) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `satuan` varchar(111) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_barang_harga`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 65 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_barang_harga
-- ----------------------------
INSERT INTO `mu_barang_harga` VALUES (1, 1, 1, 1, '180000', '20', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (2, 1, 1, 2, '180000', '20', '0', '0', 'bks');
INSERT INTO `mu_barang_harga` VALUES (3, 1, 1, 3, '180000', '20', '0', '0', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (4, 1, 1, 4, '180000', '20', '0', '0', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (5, 1, 2, 1, '3000', '0', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (6, 1, 2, 2, '3000', '0', '0', '0', 'bks');
INSERT INTO `mu_barang_harga` VALUES (7, 1, 2, 3, '3000', '0', '0', '0', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (8, 1, 2, 4, '3000', '0', '0', '0', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (9, 1, 3, 1, '3000', '0', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (10, 1, 3, 2, '3000', '0', '0', '0', 'bks');
INSERT INTO `mu_barang_harga` VALUES (11, 1, 3, 3, '3000', '0', '0', '0', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (12, 1, 3, 4, '3000', '0', '0', '0', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (13, 2, 1, 1, '2400', '13', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (14, 2, 1, 2, '2250', '8', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (15, 2, 1, 3, '2200', '7', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (16, 2, 1, 4, '2300', '-75', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (17, 2, 2, 1, '2250', '-75', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (18, 2, 2, 2, '2200', '-75.384615384615', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (19, 2, 2, 3, '2300', '-74.615384615385', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (20, 2, 2, 4, '2250', '-75', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (21, 2, 3, 1, '2250', '-75', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (22, 2, 3, 2, '2200', '-75.384615384615', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (23, 2, 3, 3, '2300', '-74.615384615385', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (24, 2, 3, 4, '2250', '-75', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (37, 4, 1, 1, '2500', '9', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (25, 3, 1, 1, '2400', '60', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (26, 3, 1, 2, '2250', '50', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (27, 3, 1, 3, '2200', '47', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (28, 3, 1, 4, '2300', '53', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (29, 3, 2, 1, '2250', '50', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (30, 3, 2, 2, '2200', '47', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (31, 3, 2, 3, '2300', '53', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (32, 3, 2, 4, '2250', '50', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (33, 3, 3, 1, '2250', '50', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (34, 3, 3, 2, '2200', '47', '0', '3', 'bks');
INSERT INTO `mu_barang_harga` VALUES (35, 3, 3, 3, '2300', '53', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (36, 3, 3, 4, '2250', '50', '0', '20', 'kodi');
INSERT INTO `mu_barang_harga` VALUES (38, 4, 1, 2, '2400', '4', '0', '12', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (39, 4, 1, 3, '0', '0', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (40, 4, 1, 4, '0', '0', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (41, 4, 2, 1, '', '', '', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (42, 4, 2, 2, '', '', '', '', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (43, 4, 2, 3, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (44, 4, 2, 4, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (45, 4, 3, 1, '', '', '', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (46, 4, 3, 2, '', '', '', '', 'lsn');
INSERT INTO `mu_barang_harga` VALUES (47, 4, 3, 3, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (48, 4, 3, 4, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (49, 5, 1, 1, '15000', '50', '0', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (50, 5, 1, 2, '0', '0', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (51, 5, 1, 3, '0', '0', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (52, 5, 1, 4, '0', '0', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (53, 6, 1, 1, '35000', '', '', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (54, 6, 1, 2, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (55, 6, 1, 3, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (56, 6, 1, 4, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (57, 7, 1, 1, '25000', '14', '', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (58, 7, 1, 2, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (59, 7, 1, 3, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (60, 7, 1, 4, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (61, 8, 1, 1, '175500', '17', '', '1', 'bh');
INSERT INTO `mu_barang_harga` VALUES (62, 8, 1, 2, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (63, 8, 1, 3, '', '', '', '', '');
INSERT INTO `mu_barang_harga` VALUES (64, 8, 1, 4, '', '', '', '', '');

-- ----------------------------
-- Table structure for mu_barang_jenis_jual
-- ----------------------------
DROP TABLE IF EXISTS `mu_barang_jenis_jual`;
CREATE TABLE `mu_barang_jenis_jual`  (
  `id_barang_jenis_jual` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_jual` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_barang_jenis_jual`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_barang_jenis_jual
-- ----------------------------
INSERT INTO `mu_barang_jenis_jual` VALUES (1, 'Ritel');
INSERT INTO `mu_barang_jenis_jual` VALUES (2, 'Grosir 1');
INSERT INTO `mu_barang_jenis_jual` VALUES (3, 'Grosir 2');
INSERT INTO `mu_barang_jenis_jual` VALUES (4, 'Grosir 3');

-- ----------------------------
-- Table structure for mu_bebanbiaya_list
-- ----------------------------
DROP TABLE IF EXISTS `mu_bebanbiaya_list`;
CREATE TABLE `mu_bebanbiaya_list`  (
  `id_bebanbiaya_list` int(11) NOT NULL AUTO_INCREMENT,
  `id_bebanbiaya_sub` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah_uang` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `waktu_proses` datetime(0) NOT NULL,
  PRIMARY KEY (`id_bebanbiaya_list`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_bebanbiaya_list
-- ----------------------------
INSERT INTO `mu_bebanbiaya_list` VALUES (1, 7, '2017-03-01', 250000, 'Sudah dibayarkan,..', 1, '2017-03-07 09:19:15');
INSERT INTO `mu_bebanbiaya_list` VALUES (2, 1, '2017-03-02', 1500000, 'Sudah dibayarkan,..', 1, '2017-03-07 13:59:13');

-- ----------------------------
-- Table structure for mu_bebanbiaya_main
-- ----------------------------
DROP TABLE IF EXISTS `mu_bebanbiaya_main`;
CREATE TABLE `mu_bebanbiaya_main`  (
  `id_bebanbiaya_main` int(11) NOT NULL AUTO_INCREMENT,
  `nama_bebanbiaya_main` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_bebanbiaya_main`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_bebanbiaya_main
-- ----------------------------
INSERT INTO `mu_bebanbiaya_main` VALUES (1, 'Beban Usaha atau Operasi');
INSERT INTO `mu_bebanbiaya_main` VALUES (2, 'Beban Penjualan');
INSERT INTO `mu_bebanbiaya_main` VALUES (3, 'Beban Lain-lain');

-- ----------------------------
-- Table structure for mu_bebanbiaya_sub
-- ----------------------------
DROP TABLE IF EXISTS `mu_bebanbiaya_sub`;
CREATE TABLE `mu_bebanbiaya_sub`  (
  `id_bebanbiaya_sub` int(11) NOT NULL AUTO_INCREMENT,
  `id_bebanbiaya_main` int(11) NOT NULL,
  `nama_bebanbiaya_sub` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_bebanbiaya_sub`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_bebanbiaya_sub
-- ----------------------------
INSERT INTO `mu_bebanbiaya_sub` VALUES (1, 1, 'Beban Gaji Karyawan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (2, 1, 'Beban Biaya Pengiriman');
INSERT INTO `mu_bebanbiaya_sub` VALUES (3, 1, 'Beban Listrik, Telepon dan Air');
INSERT INTO `mu_bebanbiaya_sub` VALUES (4, 1, 'Beban Angkutan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (5, 1, 'Beban Perlengkapan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (6, 2, 'Return Penjualan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (7, 2, 'Beban Iklan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (8, 3, 'Beban Pajak Penjualan');
INSERT INTO `mu_bebanbiaya_sub` VALUES (9, 3, 'Beban Bunga');

-- ----------------------------
-- Table structure for mu_city
-- ----------------------------
DROP TABLE IF EXISTS `mu_city`;
CREATE TABLE `mu_city`  (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`city_id`) USING BTREE,
  INDEX `fk_city_state`(`state_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2023 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_city
-- ----------------------------
INSERT INTO `mu_city` VALUES (1, 1, 'Badung', 1);
INSERT INTO `mu_city` VALUES (2, 1, 'Bangli', 1);
INSERT INTO `mu_city` VALUES (3, 1, 'Buleleng', 1);
INSERT INTO `mu_city` VALUES (4, 1, 'Gianyar', 1);
INSERT INTO `mu_city` VALUES (5, 1, 'Jembrana', 1);
INSERT INTO `mu_city` VALUES (6, 1, 'Karangasem', 1);
INSERT INTO `mu_city` VALUES (7, 1, 'Klungkung', 1);
INSERT INTO `mu_city` VALUES (8, 1, 'Tabanan', 1);
INSERT INTO `mu_city` VALUES (9, 1, 'Denpasar', 1);
INSERT INTO `mu_city` VALUES (10, 2, 'Bengkulu Selatan', 1);
INSERT INTO `mu_city` VALUES (11, 2, 'Bengkulu Utara', 1);
INSERT INTO `mu_city` VALUES (12, 2, 'Kaur', 1);
INSERT INTO `mu_city` VALUES (13, 2, 'Kepahiang', 1);
INSERT INTO `mu_city` VALUES (14, 2, 'Lebong', 1);
INSERT INTO `mu_city` VALUES (15, 2, 'Muko-Muko', 1);
INSERT INTO `mu_city` VALUES (16, 2, 'Rejang Lebong', 1);
INSERT INTO `mu_city` VALUES (17, 2, 'Seluma', 1);
INSERT INTO `mu_city` VALUES (18, 2, 'Bengkulu', 1);
INSERT INTO `mu_city` VALUES (19, 3, 'Lebak', 1);
INSERT INTO `mu_city` VALUES (20, 3, 'Pandeglang', 1);
INSERT INTO `mu_city` VALUES (21, 3, 'Serang', 1);
INSERT INTO `mu_city` VALUES (22, 3, 'Tangerang', 1);
INSERT INTO `mu_city` VALUES (23, 3, 'Cilegon', 1);
INSERT INTO `mu_city` VALUES (24, 3, 'Tangerang', 1);
INSERT INTO `mu_city` VALUES (25, 4, 'Bantul', 1);
INSERT INTO `mu_city` VALUES (26, 4, 'Gunung Kidul', 1);
INSERT INTO `mu_city` VALUES (27, 4, 'Kulon Progo', 1);
INSERT INTO `mu_city` VALUES (28, 4, 'Sleman', 1);
INSERT INTO `mu_city` VALUES (29, 4, 'Yogyakarta', 1);
INSERT INTO `mu_city` VALUES (30, 5, 'Boalemo', 1);
INSERT INTO `mu_city` VALUES (31, 5, 'Bone Bolango', 1);
INSERT INTO `mu_city` VALUES (32, 5, 'Gorontalo', 1);
INSERT INTO `mu_city` VALUES (33, 5, 'Pohuwato', 1);
INSERT INTO `mu_city` VALUES (34, 6, 'Fak-fak', 1);
INSERT INTO `mu_city` VALUES (35, 6, 'Kaimana', 1);
INSERT INTO `mu_city` VALUES (36, 6, 'Monakwari', 1);
INSERT INTO `mu_city` VALUES (37, 6, 'Raja Ampat', 1);
INSERT INTO `mu_city` VALUES (38, 6, 'Sorong', 1);
INSERT INTO `mu_city` VALUES (39, 6, 'Sorong Selatan', 1);
INSERT INTO `mu_city` VALUES (40, 6, 'Teluk Bintuni', 1);
INSERT INTO `mu_city` VALUES (41, 6, 'Teluk Wandama', 1);
INSERT INTO `mu_city` VALUES (42, 7, 'Jakarta Barat', 1);
INSERT INTO `mu_city` VALUES (43, 7, 'Jakarta Pusat', 1);
INSERT INTO `mu_city` VALUES (44, 7, 'Jakarta Utara', 1);
INSERT INTO `mu_city` VALUES (45, 7, 'Jakarta Timur', 1);
INSERT INTO `mu_city` VALUES (46, 7, 'Jakarta Selatan', 1);
INSERT INTO `mu_city` VALUES (47, 7, 'Kepulauan Seribu', 1);
INSERT INTO `mu_city` VALUES (48, 8, 'Batang Hari', 1);
INSERT INTO `mu_city` VALUES (49, 8, 'Bungo', 1);
INSERT INTO `mu_city` VALUES (50, 8, 'Kerinci', 1);
INSERT INTO `mu_city` VALUES (51, 8, 'Merangin', 1);
INSERT INTO `mu_city` VALUES (52, 8, 'Muaro Jambi', 1);
INSERT INTO `mu_city` VALUES (53, 8, 'Sarolangun', 1);
INSERT INTO `mu_city` VALUES (54, 8, 'Tanjung Jabung Timur', 1);
INSERT INTO `mu_city` VALUES (55, 8, 'Tanjung Jabung Barat', 1);
INSERT INTO `mu_city` VALUES (56, 8, 'Tebo', 1);
INSERT INTO `mu_city` VALUES (57, 8, 'Jambi', 1);
INSERT INTO `mu_city` VALUES (58, 9, 'Bandung', 1);
INSERT INTO `mu_city` VALUES (59, 9, 'Bekasi', 1);
INSERT INTO `mu_city` VALUES (60, 9, 'Bogor', 1);
INSERT INTO `mu_city` VALUES (61, 9, 'Ciamis', 1);
INSERT INTO `mu_city` VALUES (62, 9, 'Cianjur', 1);
INSERT INTO `mu_city` VALUES (63, 9, 'Cirebon', 1);
INSERT INTO `mu_city` VALUES (64, 9, 'Garut', 1);
INSERT INTO `mu_city` VALUES (65, 9, 'Indramayu', 1);
INSERT INTO `mu_city` VALUES (66, 9, 'Karawang', 1);
INSERT INTO `mu_city` VALUES (67, 9, 'Kuningan', 1);
INSERT INTO `mu_city` VALUES (68, 9, 'Majanlengka', 1);
INSERT INTO `mu_city` VALUES (69, 9, 'Purwakarta', 1);
INSERT INTO `mu_city` VALUES (70, 9, 'Subang', 1);
INSERT INTO `mu_city` VALUES (71, 9, 'Sukabumi', 1);
INSERT INTO `mu_city` VALUES (72, 9, 'Sumedang', 1);
INSERT INTO `mu_city` VALUES (73, 9, 'Tasikmalaya', 1);
INSERT INTO `mu_city` VALUES (74, 9, 'Banjar', 1);
INSERT INTO `mu_city` VALUES (75, 9, 'Cimahi', 1);
INSERT INTO `mu_city` VALUES (76, 9, 'Depok', 1);
INSERT INTO `mu_city` VALUES (77, 10, 'Banjarnegara', 1);
INSERT INTO `mu_city` VALUES (78, 10, 'Banyumas', 1);
INSERT INTO `mu_city` VALUES (79, 10, 'Batang', 1);
INSERT INTO `mu_city` VALUES (80, 10, 'Blora', 1);
INSERT INTO `mu_city` VALUES (81, 10, 'Boyolali', 1);
INSERT INTO `mu_city` VALUES (82, 10, 'Brebes', 1);
INSERT INTO `mu_city` VALUES (83, 10, 'Cilacap', 1);
INSERT INTO `mu_city` VALUES (84, 10, 'Demak', 1);
INSERT INTO `mu_city` VALUES (85, 10, 'Grobogan', 1);
INSERT INTO `mu_city` VALUES (86, 10, 'Jepara', 1);
INSERT INTO `mu_city` VALUES (87, 10, 'Karanganyar', 1);
INSERT INTO `mu_city` VALUES (88, 10, 'Kebumen', 1);
INSERT INTO `mu_city` VALUES (89, 10, 'Kendal', 1);
INSERT INTO `mu_city` VALUES (90, 10, 'Klaten', 1);
INSERT INTO `mu_city` VALUES (91, 10, 'Kudus', 1);
INSERT INTO `mu_city` VALUES (92, 10, 'Magelang', 1);
INSERT INTO `mu_city` VALUES (93, 10, 'Pati', 1);
INSERT INTO `mu_city` VALUES (94, 10, 'Pekalongan', 1);
INSERT INTO `mu_city` VALUES (95, 10, 'Pemalang', 1);
INSERT INTO `mu_city` VALUES (96, 10, 'Purbalingga', 1);
INSERT INTO `mu_city` VALUES (97, 10, 'Purworejo', 1);
INSERT INTO `mu_city` VALUES (98, 10, 'Rembang', 1);
INSERT INTO `mu_city` VALUES (99, 10, 'Semarang', 1);
INSERT INTO `mu_city` VALUES (100, 10, 'Sragen', 1);
INSERT INTO `mu_city` VALUES (101, 10, 'Sukoharjo', 1);
INSERT INTO `mu_city` VALUES (102, 10, 'Tegal', 1);
INSERT INTO `mu_city` VALUES (103, 10, 'Temanggung', 1);
INSERT INTO `mu_city` VALUES (104, 10, 'Wonogiri', 1);
INSERT INTO `mu_city` VALUES (105, 10, 'Wonosobo', 1);
INSERT INTO `mu_city` VALUES (106, 11, 'Bangakalan', 1);
INSERT INTO `mu_city` VALUES (107, 11, 'Banyuwangi', 1);
INSERT INTO `mu_city` VALUES (108, 11, 'Blitar', 1);
INSERT INTO `mu_city` VALUES (109, 11, 'Bojonegoro', 1);
INSERT INTO `mu_city` VALUES (110, 11, 'Bondowoso', 1);
INSERT INTO `mu_city` VALUES (111, 11, 'Gresik', 1);
INSERT INTO `mu_city` VALUES (112, 11, 'Jember', 1);
INSERT INTO `mu_city` VALUES (113, 11, 'Jombang', 1);
INSERT INTO `mu_city` VALUES (114, 11, 'Kediri', 1);
INSERT INTO `mu_city` VALUES (115, 11, 'Lamongan', 1);
INSERT INTO `mu_city` VALUES (116, 11, 'Lumajang', 1);
INSERT INTO `mu_city` VALUES (117, 11, 'Madiun', 1);
INSERT INTO `mu_city` VALUES (118, 11, 'Magetan', 1);
INSERT INTO `mu_city` VALUES (119, 11, 'Malang', 1);
INSERT INTO `mu_city` VALUES (120, 11, 'Mojokerto', 1);
INSERT INTO `mu_city` VALUES (121, 11, 'Nganjuk', 1);
INSERT INTO `mu_city` VALUES (122, 11, 'Ngawi', 1);
INSERT INTO `mu_city` VALUES (123, 11, 'Pacitan', 1);
INSERT INTO `mu_city` VALUES (124, 11, 'Pamekasan', 1);
INSERT INTO `mu_city` VALUES (125, 11, 'Pasuruan', 1);
INSERT INTO `mu_city` VALUES (126, 11, 'Ponorogo', 1);
INSERT INTO `mu_city` VALUES (127, 11, 'Probolinggo', 1);
INSERT INTO `mu_city` VALUES (128, 11, 'Sampang', 1);
INSERT INTO `mu_city` VALUES (129, 11, 'Sidoarjo', 1);
INSERT INTO `mu_city` VALUES (130, 11, 'Situbondo', 1);
INSERT INTO `mu_city` VALUES (131, 11, 'Sumenep', 1);
INSERT INTO `mu_city` VALUES (132, 11, 'Trenggalek', 1);
INSERT INTO `mu_city` VALUES (133, 11, 'Tuban', 1);
INSERT INTO `mu_city` VALUES (134, 11, 'Tulungagung', 1);
INSERT INTO `mu_city` VALUES (135, 11, 'Batu', 1);
INSERT INTO `mu_city` VALUES (136, 11, 'Surabaya', 1);
INSERT INTO `mu_city` VALUES (137, 12, 'Bengkayang', 1);
INSERT INTO `mu_city` VALUES (138, 12, 'Kapuas Hulu', 1);
INSERT INTO `mu_city` VALUES (139, 12, 'Ketapang', 1);
INSERT INTO `mu_city` VALUES (140, 12, 'Landak', 1);
INSERT INTO `mu_city` VALUES (141, 12, 'Melawi', 1);
INSERT INTO `mu_city` VALUES (142, 12, 'Pontianak', 1);
INSERT INTO `mu_city` VALUES (143, 12, 'Sambas', 1);
INSERT INTO `mu_city` VALUES (144, 12, 'Sanggau', 1);
INSERT INTO `mu_city` VALUES (145, 12, 'Sukadau', 1);
INSERT INTO `mu_city` VALUES (146, 12, 'Sintang', 1);
INSERT INTO `mu_city` VALUES (147, 13, 'Barito Selatan', 1);
INSERT INTO `mu_city` VALUES (148, 13, 'Barito Timur', 1);
INSERT INTO `mu_city` VALUES (149, 13, 'Barito Utara', 1);
INSERT INTO `mu_city` VALUES (150, 13, 'Gunung Mas', 1);
INSERT INTO `mu_city` VALUES (151, 13, 'Kapuas', 1);
INSERT INTO `mu_city` VALUES (152, 13, 'Katingan', 1);
INSERT INTO `mu_city` VALUES (153, 13, 'Kotawaringin Barat', 1);
INSERT INTO `mu_city` VALUES (154, 13, 'Kotawaringin Timur', 1);
INSERT INTO `mu_city` VALUES (155, 13, 'Lamandayu', 1);
INSERT INTO `mu_city` VALUES (156, 13, 'Murung Raya', 1);
INSERT INTO `mu_city` VALUES (157, 13, 'Pulang Pisau', 1);
INSERT INTO `mu_city` VALUES (158, 13, 'Sukamara', 1);
INSERT INTO `mu_city` VALUES (159, 13, 'Seruyan', 1);
INSERT INTO `mu_city` VALUES (160, 13, 'Palangka Raya', 1);
INSERT INTO `mu_city` VALUES (161, 14, 'Berau', 1);
INSERT INTO `mu_city` VALUES (162, 14, 'Bulungan', 1);
INSERT INTO `mu_city` VALUES (163, 14, 'Kutai Barat', 1);
INSERT INTO `mu_city` VALUES (164, 14, 'Kutai Kertanegara', 1);
INSERT INTO `mu_city` VALUES (165, 14, 'Kutai Timur', 1);
INSERT INTO `mu_city` VALUES (166, 14, 'Malinau', 1);
INSERT INTO `mu_city` VALUES (167, 14, 'Nunukan', 1);
INSERT INTO `mu_city` VALUES (168, 14, 'Pasir', 1);
INSERT INTO `mu_city` VALUES (169, 14, 'Penajam Paser Utara', 1);
INSERT INTO `mu_city` VALUES (170, 14, 'Balikpapan', 1);
INSERT INTO `mu_city` VALUES (171, 14, 'Bontang', 1);
INSERT INTO `mu_city` VALUES (172, 14, 'Samarinda', 1);
INSERT INTO `mu_city` VALUES (173, 14, 'Tarakan', 1);
INSERT INTO `mu_city` VALUES (174, 15, 'Balangan', 1);
INSERT INTO `mu_city` VALUES (175, 15, 'Banjar', 1);
INSERT INTO `mu_city` VALUES (176, 15, 'Barito Kuala', 1);
INSERT INTO `mu_city` VALUES (177, 15, 'Hulu Sungai Selatan', 1);
INSERT INTO `mu_city` VALUES (178, 15, 'Hulu Sungai Tengah', 1);
INSERT INTO `mu_city` VALUES (179, 15, 'Hulu Sungai Utara', 1);
INSERT INTO `mu_city` VALUES (180, 15, 'Kota Baru', 1);
INSERT INTO `mu_city` VALUES (181, 15, 'Kota Laut', 1);
INSERT INTO `mu_city` VALUES (182, 15, 'Tabalong', 1);
INSERT INTO `mu_city` VALUES (183, 15, 'Tanah Bambu', 1);
INSERT INTO `mu_city` VALUES (184, 15, 'Tapin', 1);
INSERT INTO `mu_city` VALUES (185, 15, 'Banjarbaru', 1);
INSERT INTO `mu_city` VALUES (186, 15, 'Banjarmasin', 1);
INSERT INTO `mu_city` VALUES (187, 16, 'Bangka', 1);
INSERT INTO `mu_city` VALUES (188, 16, 'Bangka Barat', 1);
INSERT INTO `mu_city` VALUES (189, 16, 'Bangka Tengah', 1);
INSERT INTO `mu_city` VALUES (190, 16, 'Bangka Selatan', 1);
INSERT INTO `mu_city` VALUES (191, 16, 'Belitung', 1);
INSERT INTO `mu_city` VALUES (192, 16, 'Belitung Timur', 1);
INSERT INTO `mu_city` VALUES (193, 16, 'Pangkal Pinang', 1);
INSERT INTO `mu_city` VALUES (194, 17, 'Lampung Barat', 1);
INSERT INTO `mu_city` VALUES (195, 17, 'Lampung Selatan', 1);
INSERT INTO `mu_city` VALUES (196, 17, 'Lampung Tengah', 1);
INSERT INTO `mu_city` VALUES (197, 17, 'Lampung Timur', 1);
INSERT INTO `mu_city` VALUES (198, 17, 'Lampung Utara', 1);
INSERT INTO `mu_city` VALUES (199, 17, 'Way Kanan', 1);
INSERT INTO `mu_city` VALUES (200, 17, 'Tanggamus', 1);
INSERT INTO `mu_city` VALUES (201, 17, 'Tulang Bawang', 1);
INSERT INTO `mu_city` VALUES (202, 17, 'Bandar Lampung', 1);
INSERT INTO `mu_city` VALUES (203, 17, 'Metro', 1);
INSERT INTO `mu_city` VALUES (204, 18, 'Buru', 1);
INSERT INTO `mu_city` VALUES (205, 18, 'Kepulauan Aru', 1);
INSERT INTO `mu_city` VALUES (206, 18, 'Maluku Tengah', 1);
INSERT INTO `mu_city` VALUES (207, 18, 'Maluku Tenggara', 1);
INSERT INTO `mu_city` VALUES (208, 18, 'Maluku Tenggara Barat', 1);
INSERT INTO `mu_city` VALUES (209, 18, 'Seram Bagian Barat', 1);
INSERT INTO `mu_city` VALUES (210, 18, 'Seram Bagian Timur', 1);
INSERT INTO `mu_city` VALUES (211, 18, 'Ambon', 1);
INSERT INTO `mu_city` VALUES (212, 19, 'Halmahera Barat', 1);
INSERT INTO `mu_city` VALUES (213, 19, 'Halmahera Selatan', 1);
INSERT INTO `mu_city` VALUES (214, 19, 'Halmahera Tengah', 1);
INSERT INTO `mu_city` VALUES (215, 19, 'Halmahera Timur', 1);
INSERT INTO `mu_city` VALUES (216, 19, 'Halmahera Utara', 1);
INSERT INTO `mu_city` VALUES (217, 19, 'Kepulauan Sula', 1);
INSERT INTO `mu_city` VALUES (218, 19, 'Ternate', 1);
INSERT INTO `mu_city` VALUES (219, 19, 'Tidore', 1);
INSERT INTO `mu_city` VALUES (220, 20, 'Aceh Barat', 1);
INSERT INTO `mu_city` VALUES (221, 20, 'Aceh Barat Daya', 1);
INSERT INTO `mu_city` VALUES (222, 20, 'Aceh Besar', 1);
INSERT INTO `mu_city` VALUES (223, 20, 'Aceh Jaya', 1);
INSERT INTO `mu_city` VALUES (224, 20, 'Aceh Selatan', 1);
INSERT INTO `mu_city` VALUES (225, 20, 'Aceh Singkil', 1);
INSERT INTO `mu_city` VALUES (226, 20, 'Aceh Tamiang', 1);
INSERT INTO `mu_city` VALUES (227, 20, 'Aceh Tengah', 1);
INSERT INTO `mu_city` VALUES (228, 20, 'Aceh Tenggara', 1);
INSERT INTO `mu_city` VALUES (229, 20, 'Aceh Timur', 1);
INSERT INTO `mu_city` VALUES (230, 20, 'Aceh Utara', 1);
INSERT INTO `mu_city` VALUES (231, 20, 'Bener Meriah', 1);
INSERT INTO `mu_city` VALUES (232, 20, 'Bireuen', 1);
INSERT INTO `mu_city` VALUES (233, 20, 'Gayo Lues', 1);
INSERT INTO `mu_city` VALUES (234, 20, 'Nagan Raya', 1);
INSERT INTO `mu_city` VALUES (235, 20, 'Pidie', 1);
INSERT INTO `mu_city` VALUES (236, 20, 'Simeulue', 1);
INSERT INTO `mu_city` VALUES (237, 20, 'Banda Aceh', 1);
INSERT INTO `mu_city` VALUES (238, 20, 'Langsa', 1);
INSERT INTO `mu_city` VALUES (239, 20, 'Lhokseumawe', 1);
INSERT INTO `mu_city` VALUES (240, 20, 'Sabang', 1);
INSERT INTO `mu_city` VALUES (241, 21, 'Bima', 1);
INSERT INTO `mu_city` VALUES (242, 21, 'Dompu', 1);
INSERT INTO `mu_city` VALUES (243, 21, 'Lombok Barat', 1);
INSERT INTO `mu_city` VALUES (244, 21, 'Lombok Tengah', 1);
INSERT INTO `mu_city` VALUES (245, 21, 'Lombok Timur', 1);
INSERT INTO `mu_city` VALUES (246, 21, 'Sumbawa', 1);
INSERT INTO `mu_city` VALUES (247, 21, 'Sumbawa Barat', 1);
INSERT INTO `mu_city` VALUES (248, 21, 'Mataram', 1);
INSERT INTO `mu_city` VALUES (249, 22, 'Alor', 1);
INSERT INTO `mu_city` VALUES (250, 22, 'Belu', 1);
INSERT INTO `mu_city` VALUES (251, 22, 'Ende', 1);
INSERT INTO `mu_city` VALUES (252, 22, 'Flores Timur', 1);
INSERT INTO `mu_city` VALUES (253, 22, 'Kupang', 1);
INSERT INTO `mu_city` VALUES (254, 22, 'Lembata', 1);
INSERT INTO `mu_city` VALUES (255, 22, 'Manggarai', 1);
INSERT INTO `mu_city` VALUES (256, 22, 'Manggarai Barat', 1);
INSERT INTO `mu_city` VALUES (257, 22, 'Ngada', 1);
INSERT INTO `mu_city` VALUES (258, 22, 'Rote Ndau', 1);
INSERT INTO `mu_city` VALUES (259, 22, 'Sikka', 1);
INSERT INTO `mu_city` VALUES (260, 22, 'Sumba Barat', 1);
INSERT INTO `mu_city` VALUES (261, 22, 'Sumba Timur', 1);
INSERT INTO `mu_city` VALUES (262, 22, 'Timor Tengah Selatan', 1);
INSERT INTO `mu_city` VALUES (263, 22, 'Timor Tengah Utara', 1);
INSERT INTO `mu_city` VALUES (264, 22, 'Kupang', 1);
INSERT INTO `mu_city` VALUES (265, 23, 'Asmat', 1);
INSERT INTO `mu_city` VALUES (266, 23, 'Biak Numfor', 1);
INSERT INTO `mu_city` VALUES (267, 23, 'Boven Digoel', 1);
INSERT INTO `mu_city` VALUES (268, 23, 'Jayapura', 1);
INSERT INTO `mu_city` VALUES (269, 23, 'Jayawijaya', 1);
INSERT INTO `mu_city` VALUES (270, 23, 'Keeron', 1);
INSERT INTO `mu_city` VALUES (271, 23, 'Mappi', 1);
INSERT INTO `mu_city` VALUES (272, 23, 'Merauke', 1);
INSERT INTO `mu_city` VALUES (273, 23, 'Mimika', 1);
INSERT INTO `mu_city` VALUES (274, 23, 'Nabire', 1);
INSERT INTO `mu_city` VALUES (275, 23, 'Paniai', 1);
INSERT INTO `mu_city` VALUES (276, 23, 'Pegunungan Bintang', 1);
INSERT INTO `mu_city` VALUES (277, 23, 'Puncak Jaya', 1);
INSERT INTO `mu_city` VALUES (278, 23, 'Sarmi', 1);
INSERT INTO `mu_city` VALUES (279, 23, 'Sapiori', 1);
INSERT INTO `mu_city` VALUES (280, 23, 'Tolikara', 1);
INSERT INTO `mu_city` VALUES (281, 23, 'Waropen', 1);
INSERT INTO `mu_city` VALUES (282, 23, 'Yahukimo', 1);
INSERT INTO `mu_city` VALUES (283, 23, 'Yapen Waropen', 1);
INSERT INTO `mu_city` VALUES (284, 24, 'Bengkalis', 1);
INSERT INTO `mu_city` VALUES (285, 24, 'Indragiri Hilir', 1);
INSERT INTO `mu_city` VALUES (286, 24, 'Indragiri Hulu', 1);
INSERT INTO `mu_city` VALUES (287, 24, 'Kampar', 1);
INSERT INTO `mu_city` VALUES (288, 24, 'Kuantan Singingi', 1);
INSERT INTO `mu_city` VALUES (289, 24, 'Pelalawan', 1);
INSERT INTO `mu_city` VALUES (290, 24, 'Rokan Hulu', 1);
INSERT INTO `mu_city` VALUES (291, 24, 'Rokan Hilir', 1);
INSERT INTO `mu_city` VALUES (292, 24, 'Siak', 1);
INSERT INTO `mu_city` VALUES (293, 24, 'Dumai', 1);
INSERT INTO `mu_city` VALUES (294, 24, 'Pekanbaru', 1);
INSERT INTO `mu_city` VALUES (295, 25, 'Karimun', 1);
INSERT INTO `mu_city` VALUES (296, 25, 'Bintan', 1);
INSERT INTO `mu_city` VALUES (297, 25, 'Lingga', 1);
INSERT INTO `mu_city` VALUES (298, 25, 'Natuna', 1);
INSERT INTO `mu_city` VALUES (299, 25, 'Batam', 1);
INSERT INTO `mu_city` VALUES (300, 25, 'Tanjung Pinang', 1);
INSERT INTO `mu_city` VALUES (301, 26, 'Majene', 1);
INSERT INTO `mu_city` VALUES (302, 26, 'Mamasa', 1);
INSERT INTO `mu_city` VALUES (303, 26, 'Mamuju', 1);
INSERT INTO `mu_city` VALUES (304, 26, 'Mamuju Utara', 1);
INSERT INTO `mu_city` VALUES (305, 26, 'Polewali Mandar', 1);
INSERT INTO `mu_city` VALUES (306, 27, 'Banggai', 1);
INSERT INTO `mu_city` VALUES (307, 27, 'Banggai Kepulauan', 1);
INSERT INTO `mu_city` VALUES (308, 27, 'Buol', 1);
INSERT INTO `mu_city` VALUES (309, 27, 'Donggala', 1);
INSERT INTO `mu_city` VALUES (310, 27, 'Morowali', 1);
INSERT INTO `mu_city` VALUES (311, 27, 'Pirigi Moutong', 1);
INSERT INTO `mu_city` VALUES (312, 27, 'Poso', 1);
INSERT INTO `mu_city` VALUES (313, 27, 'Tojo Una-Una', 1);
INSERT INTO `mu_city` VALUES (314, 27, 'Toli-Toli', 1);
INSERT INTO `mu_city` VALUES (315, 27, 'Palu', 1);
INSERT INTO `mu_city` VALUES (316, 28, 'Bombana', 1);
INSERT INTO `mu_city` VALUES (317, 28, 'Buton', 1);
INSERT INTO `mu_city` VALUES (318, 28, 'Kolaka', 1);
INSERT INTO `mu_city` VALUES (319, 28, 'Kolaka Utara', 1);
INSERT INTO `mu_city` VALUES (320, 28, 'Konawe', 1);
INSERT INTO `mu_city` VALUES (321, 28, 'Konawe Selatan', 1);
INSERT INTO `mu_city` VALUES (322, 28, 'Muna', 1);
INSERT INTO `mu_city` VALUES (323, 28, 'Wakatobi', 1);
INSERT INTO `mu_city` VALUES (324, 28, 'Bau-Bau', 1);
INSERT INTO `mu_city` VALUES (325, 28, 'Kendari', 1);
INSERT INTO `mu_city` VALUES (326, 29, 'Bantaeng', 1);
INSERT INTO `mu_city` VALUES (327, 29, 'Barru', 1);
INSERT INTO `mu_city` VALUES (328, 29, 'Bone', 1);
INSERT INTO `mu_city` VALUES (329, 29, 'Bulukumba', 1);
INSERT INTO `mu_city` VALUES (330, 29, 'Enrekang', 1);
INSERT INTO `mu_city` VALUES (331, 29, 'Gowa', 1);
INSERT INTO `mu_city` VALUES (332, 29, 'Jeneponto', 1);
INSERT INTO `mu_city` VALUES (333, 29, 'Luwu', 1);
INSERT INTO `mu_city` VALUES (334, 29, 'Luwu Timur', 1);
INSERT INTO `mu_city` VALUES (335, 29, 'Luwu Utara', 1);
INSERT INTO `mu_city` VALUES (336, 29, 'Maros', 1);
INSERT INTO `mu_city` VALUES (337, 29, 'Pangkajene Kepulauan', 1);
INSERT INTO `mu_city` VALUES (338, 29, 'Pinrang', 1);
INSERT INTO `mu_city` VALUES (339, 29, 'Selayar', 1);
INSERT INTO `mu_city` VALUES (340, 29, 'Sinjai', 1);
INSERT INTO `mu_city` VALUES (341, 29, 'Sidenreng Rappang', 1);
INSERT INTO `mu_city` VALUES (342, 29, 'Soppeng', 1);
INSERT INTO `mu_city` VALUES (343, 29, 'Takalar', 1);
INSERT INTO `mu_city` VALUES (344, 29, 'Tana Toraja', 1);
INSERT INTO `mu_city` VALUES (345, 29, 'Wajo', 1);
INSERT INTO `mu_city` VALUES (346, 29, 'Makassar', 1);
INSERT INTO `mu_city` VALUES (347, 29, 'Palopo', 1);
INSERT INTO `mu_city` VALUES (348, 29, 'Pare-Pare', 1);
INSERT INTO `mu_city` VALUES (349, 30, 'Bolaang Mongondow', 1);
INSERT INTO `mu_city` VALUES (350, 30, 'Kepulaun Sangihe', 1);
INSERT INTO `mu_city` VALUES (351, 30, 'Kepulauan Talaud', 1);
INSERT INTO `mu_city` VALUES (352, 30, 'Minahasa', 1);
INSERT INTO `mu_city` VALUES (353, 30, 'Minahasa Selatan', 1);
INSERT INTO `mu_city` VALUES (354, 30, 'Minahasa Utara', 1);
INSERT INTO `mu_city` VALUES (355, 30, 'Bitung', 1);
INSERT INTO `mu_city` VALUES (356, 30, 'Manado', 1);
INSERT INTO `mu_city` VALUES (357, 30, 'Tomohon', 1);
INSERT INTO `mu_city` VALUES (358, 31, 'Agam', 1);
INSERT INTO `mu_city` VALUES (359, 31, 'Dharmasraya', 1);
INSERT INTO `mu_city` VALUES (360, 31, 'Limapuluh Koto', 1);
INSERT INTO `mu_city` VALUES (361, 31, 'Kepulauan Mentawai', 1);
INSERT INTO `mu_city` VALUES (362, 31, 'Padang Pariaman', 1);
INSERT INTO `mu_city` VALUES (363, 31, 'Pasaman', 1);
INSERT INTO `mu_city` VALUES (364, 31, 'Pasaman Barat', 1);
INSERT INTO `mu_city` VALUES (365, 31, 'Pesisir Selatan', 1);
INSERT INTO `mu_city` VALUES (366, 31, 'Sawahlunto Sijunjung', 1);
INSERT INTO `mu_city` VALUES (367, 31, 'Solok', 1);
INSERT INTO `mu_city` VALUES (368, 31, 'Solok Selatan', 1);
INSERT INTO `mu_city` VALUES (369, 31, 'Tanah Datar', 1);
INSERT INTO `mu_city` VALUES (370, 31, 'Bukittinggi', 1);
INSERT INTO `mu_city` VALUES (371, 31, 'Padang', 1);
INSERT INTO `mu_city` VALUES (372, 31, 'Padang Panjang', 1);
INSERT INTO `mu_city` VALUES (373, 31, 'Pariaman', 1);
INSERT INTO `mu_city` VALUES (374, 31, 'Payakumbuh', 1);
INSERT INTO `mu_city` VALUES (375, 32, 'Banyuasin', 1);
INSERT INTO `mu_city` VALUES (376, 32, 'Lahat', 1);
INSERT INTO `mu_city` VALUES (377, 32, 'Muara Enim', 1);
INSERT INTO `mu_city` VALUES (378, 32, 'Musi Banyuasin', 1);
INSERT INTO `mu_city` VALUES (379, 32, 'Musi Rawas', 1);
INSERT INTO `mu_city` VALUES (380, 32, 'Ogan Ilir', 1);
INSERT INTO `mu_city` VALUES (381, 32, 'Ogan Komering Ilir', 1);
INSERT INTO `mu_city` VALUES (382, 32, 'Ogan Komering Ulu', 1);
INSERT INTO `mu_city` VALUES (383, 32, 'Ogan Komering Ulu Timur', 1);
INSERT INTO `mu_city` VALUES (384, 32, 'Ogan Komering Ulu Selatan', 1);
INSERT INTO `mu_city` VALUES (385, 32, 'Lubuklinggau', 1);
INSERT INTO `mu_city` VALUES (386, 32, 'Pagar Alam', 1);
INSERT INTO `mu_city` VALUES (387, 32, 'Palembang', 1);
INSERT INTO `mu_city` VALUES (388, 32, 'Prabumulih', 1);
INSERT INTO `mu_city` VALUES (389, 33, 'Asahan', 1);
INSERT INTO `mu_city` VALUES (390, 33, 'Dairi', 1);
INSERT INTO `mu_city` VALUES (391, 33, 'Deli Serdang', 1);
INSERT INTO `mu_city` VALUES (392, 33, 'Humbang Hasundatan', 1);
INSERT INTO `mu_city` VALUES (393, 33, 'Karo', 1);
INSERT INTO `mu_city` VALUES (394, 33, 'Labuhan Batu', 1);
INSERT INTO `mu_city` VALUES (395, 33, 'Langkat', 1);
INSERT INTO `mu_city` VALUES (396, 33, 'Mandailing Natal', 1);
INSERT INTO `mu_city` VALUES (397, 33, 'Nias', 1);
INSERT INTO `mu_city` VALUES (398, 33, 'Nias Selatan', 1);
INSERT INTO `mu_city` VALUES (399, 33, 'Pakpak Bharat', 1);
INSERT INTO `mu_city` VALUES (400, 33, 'Samosir', 1);
INSERT INTO `mu_city` VALUES (401, 33, 'Serdang Bedagai', 1);
INSERT INTO `mu_city` VALUES (402, 33, 'Simalungun', 1);
INSERT INTO `mu_city` VALUES (403, 33, 'Tapanuli Selatan', 1);
INSERT INTO `mu_city` VALUES (404, 33, 'Tapanuli Tengah', 1);
INSERT INTO `mu_city` VALUES (405, 33, 'Tapanuli Utara', 1);
INSERT INTO `mu_city` VALUES (406, 33, 'Toba Samosir', 1);
INSERT INTO `mu_city` VALUES (407, 33, 'Binjai', 1);
INSERT INTO `mu_city` VALUES (408, 33, 'Medan', 1);
INSERT INTO `mu_city` VALUES (409, 33, 'Padang Sidempuan', 1);
INSERT INTO `mu_city` VALUES (410, 33, 'Pematangsiantar', 1);
INSERT INTO `mu_city` VALUES (411, 33, 'Sibolga', 1);
INSERT INTO `mu_city` VALUES (412, 33, 'Tanjung Balai', 1);
INSERT INTO `mu_city` VALUES (413, 33, 'Tebing Tinggi', 1);
INSERT INTO `mu_city` VALUES (2019, 10, 'Salatiga', 1);
INSERT INTO `mu_city` VALUES (2020, 23, 'Timika', 1);

-- ----------------------------
-- Table structure for mu_conf_barang
-- ----------------------------
DROP TABLE IF EXISTS `mu_conf_barang`;
CREATE TABLE `mu_conf_barang`  (
  `id_conf_barang` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` enum('default','angka') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `harga_grosir` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `grosir_berdasarkan` enum('multi_satuan','jumlah_minimal') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `harga_kategori_pelanggan` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_satuan` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `konversi_satuan_beli` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sertakan_gambar` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_conf_barang`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_conf_barang
-- ----------------------------
INSERT INTO `mu_conf_barang` VALUES (1, 'default', 'visible', 'multi_satuan', 'visible', 'bh', 'visible', 'visible');

-- ----------------------------
-- Table structure for mu_conf_penjualan
-- ----------------------------
DROP TABLE IF EXISTS `mu_conf_penjualan`;
CREATE TABLE `mu_conf_penjualan`  (
  `id_conf_penjualan` int(11) NOT NULL AUTO_INCREMENT,
  `terapkan_pajak` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `font_totalbayar_besar` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `posisi_totalbayar_besar` enum('atas','bawah') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `font_jumlahbayar_besar` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe_diskon` enum('persen','uang') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `terapkan_perubahan_diskon` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `terapkan_perubahan_harga` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `terapkan_batas_piutang` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jatuh_tempo` int(11) NOT NULL,
  `menunjang_penjualan_tunggu` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sertakan_nama_penjual` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sertakan_biaya_kirim` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diskon_agen_expadisi` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe_diskon_ekspedisi` enum('persen','uang') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diskon_untuk_pelanggan` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipe_diskon_pelanggan` enum('persen','uang') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_satuan` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan_perbarang` enum('visible','hidden') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_conf_penjualan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_conf_penjualan
-- ----------------------------
INSERT INTO `mu_conf_penjualan` VALUES (1, 'visible', 'hidden', 'atas', 'visible', 'uang', 'hidden', 'hidden', 'visible', 4, 'visible', 'hidden', 'visible', 'visible', 'persen', 'visible', 'uang', 'kodi', 'hidden');

-- ----------------------------
-- Table structure for mu_conf_perusahaan
-- ----------------------------
DROP TABLE IF EXISTS `mu_conf_perusahaan`;
CREATE TABLE `mu_conf_perusahaan`  (
  `id_perusahaan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_perusahaan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `npwp_perusahaan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_perusahaan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `telepon` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fax` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos` int(11) NOT NULL,
  `logo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_perusahaan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_conf_perusahaan
-- ----------------------------
INSERT INTO `mu_conf_perusahaan` VALUES (1, 'Bengkel Jaya Sakti', 'NPWP-452123', 'Jl. Raya Cakung', 45, 7, 1, '0751461692', 'toko@gmail.com', '0751461691', 'https://mail.google.com/mail/u/0/#inbox', 26125, 'aaa.jpg');

-- ----------------------------
-- Table structure for mu_country
-- ----------------------------
DROP TABLE IF EXISTS `mu_country`;
CREATE TABLE `mu_country`  (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`country_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_country
-- ----------------------------
INSERT INTO `mu_country` VALUES (1, 'Indonesia', 1);
INSERT INTO `mu_country` VALUES (2, 'Inggris', 1);
INSERT INTO `mu_country` VALUES (3, 'Malaysia', 1);

-- ----------------------------
-- Table structure for mu_departemen
-- ----------------------------
DROP TABLE IF EXISTS `mu_departemen`;
CREATE TABLE `mu_departemen`  (
  `id_departemen` int(11) NOT NULL AUTO_INCREMENT,
  `nama_departemen` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_departemen`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_departemen
-- ----------------------------
INSERT INTO `mu_departemen` VALUES (1, 'Direksi', 'Direksi Usaha', 1);
INSERT INTO `mu_departemen` VALUES (2, 'Keuangan', 'Bagian Keuangan', 1);

-- ----------------------------
-- Table structure for mu_jabatan
-- ----------------------------
DROP TABLE IF EXISTS `mu_jabatan`;
CREATE TABLE `mu_jabatan`  (
  `id_jabatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_jabatan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_jabatan
-- ----------------------------
INSERT INTO `mu_jabatan` VALUES (1, 'Kasir', 'Petugas Kasir Penjualan', 1);
INSERT INTO `mu_jabatan` VALUES (2, 'Pelayanan (Teknisi)', 'Orang yang melayani pelayanan atau service (jasa) seperti teknisi dan mekanik', 1);
INSERT INTO `mu_jabatan` VALUES (3, 'Pemilik', 'Owner atau Pemilik Usaha', 1);
INSERT INTO `mu_jabatan` VALUES (4, 'Penjual', 'Oarang yang menjualkan Produk seperti Salesmen atau Marketing', 1);
INSERT INTO `mu_jabatan` VALUES (6, 'Kasir', 'Kasir', 1);

-- ----------------------------
-- Table structure for mu_jenis_kelamin
-- ----------------------------
DROP TABLE IF EXISTS `mu_jenis_kelamin`;
CREATE TABLE `mu_jenis_kelamin`  (
  `id_jenis_kelamin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenis_kelamin` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_jenis_kelamin`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_jenis_kelamin
-- ----------------------------
INSERT INTO `mu_jenis_kelamin` VALUES (1, 'Laki-laki');
INSERT INTO `mu_jenis_kelamin` VALUES (2, 'Perempuan');

-- ----------------------------
-- Table structure for mu_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `mu_karyawan`;
CREATE TABLE `mu_karyawan`  (
  `id_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nik` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nama_karyawan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jenis_kelamin` int(11) NOT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `id_agama` int(11) NOT NULL,
  `id_status_pernikahan` int(11) NOT NULL,
  `alamat_karyawan_1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_karyawan_2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `telepon_karyawan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_karyawan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_karyawan` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website_karyawan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_karyawan` int(10) NOT NULL,
  `fax_karyawan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chat_karyawan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `foto_karyawan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_jabatan` int(11) NOT NULL,
  `id_departemen` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `id_status_karyawan` int(11) NOT NULL,
  `aktif` enum('ya','tidak') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_daftar` datetime(0) NOT NULL,
  PRIMARY KEY (`id_karyawan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_karyawan
-- ----------------------------
INSERT INTO `mu_karyawan` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', '547435345322', 'Administrator', 1, 'Bekasi', '1994-02-19', 1, 2, 'Jl. Susno Duadji Merere', '', 59, 9, 1, '077702154', '082145330101', 'admin@gmailc.om', 'http://www.joox.com/id/id/?channel=SEM&campaign=c%7c1047450411%7c51516498236%7ckwd-343639899335%7c295540770952%7c&gclid=Cj0KCQjw6fvdBRCbARIsABGZ-vR4Ae8uvKoaf2cc1mPQd1jHypIkGL2hT3gRoAVj10HMmKiyml3EK_EaAt7VEALw_wcB&gclsrc=aw.ds', 56789, '077702145', 'admin', '', 'download.png', 1, 2, '2016-02-12', 1, 'ya', 1, '2018-10-12 10:18:36');
INSERT INTO `mu_karyawan` VALUES (2, 'yeni', '21232f297a57a5a743894a0e4a801fc3', '997435345300', 'Yeni Ermawaty', 2, 'Padang', '1967-04-19', 1, 4, 'Jl. Angkasa Puri', '', 371, 31, 1, '0751461698', '082888661233', 'yeni.ermawaty@gmail.com', 'http://yeniermawaty.com', 56980, '0751461655', 'yeni.ermawaty@yahoo.com', '', 'yen.jpg', 4, 1, '2016-12-31', 1, 'ya', 1, '2017-01-06 15:34:24');
INSERT INTO `mu_karyawan` VALUES (3, 'mimin', '03f7f7198958ffbda01db956d15f134a', '123456', 'mimin', 2, 'Bekasi', '0000-00-00', 1, 1, 'Bekasi', '', 59, 9, 1, '28', '1', 'wadwadwd@gmail.com', 'http://www.joox.com/id/id/?channel=SEM&campaign=c%7c1047450411%7c51516498236%7ckwd-343639899335%7c295540770952%7c&gclid=Cj0KCQjw6fvdBRCbARIsABGZ-vR4Ae8uvKoaf2cc1mPQd1jHypIkGL2hT3gRoAVj10HMmKiyml3EK_EaAt7VEALw_wcB&gclsrc=aw.ds', 123, '123', '123', '123', 'bni.png', 1, 2, '0000-00-00', 2, 'ya', 1, '2018-10-27 06:55:05');

-- ----------------------------
-- Table structure for mu_kategori
-- ----------------------------
DROP TABLE IF EXISTS `mu_kategori`;
CREATE TABLE `mu_kategori`  (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_kategori`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_kategori
-- ----------------------------
INSERT INTO `mu_kategori` VALUES (1, 'Spare Part', 1);
INSERT INTO `mu_kategori` VALUES (2, 'Oli', 1);
INSERT INTO `mu_kategori` VALUES (4, 'Service', 1);

-- ----------------------------
-- Table structure for mu_kategori_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `mu_kategori_pelanggan`;
CREATE TABLE `mu_kategori_pelanggan`  (
  `id_kategori_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori_pelanggan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `permanen` enum('y','n') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'n',
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_kategori_pelanggan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_kategori_pelanggan
-- ----------------------------
INSERT INTO `mu_kategori_pelanggan` VALUES (1, 'Pelanggan Umum', 'y', 1);

-- ----------------------------
-- Table structure for mu_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `mu_pelanggan`;
CREATE TABLE `mu_pelanggan`  (
  `id_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori_pelanggan` int(11) NOT NULL,
  `id_type_pelanggan` int(11) NOT NULL,
  `nama_pelanggan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kontak_pelanggan` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_pelanggan_1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_pelanggan_2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `telpon_pelanggan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp_pelanggan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email_pelanggan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website_pelanggan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos_pelanggan` int(10) NOT NULL,
  `fax_pelanggan` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chat_pelanggan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_daftar` datetime(0) NOT NULL,
  PRIMARY KEY (`id_pelanggan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pelanggan
-- ----------------------------
INSERT INTO `mu_pelanggan` VALUES (1, 1, 1, 'Dewiit Safitri', '081265253322 (Dedew)', 'Jln, Saruko Rayo Bamato', '', 373, 31, 1, '0751223311', '085263012000', 'dewitt.safitri@gmail.com', 'http://dewiit.com', 45789, '0751220011', 'dewitt.safitri@yahoo.com', '', 1, '2016-12-31 09:13:22');
INSERT INTO `mu_pelanggan` VALUES (2, 2, 2, 'Arsenio Orlando', '082173054500', 'Jln. Ulak Karang Raya', '', 373, 31, 1, '0751635899', '082365447899', 'arsenio@gmail.com', 'http://arsenio.com', 45123, '0751567788', 'arsenio@yahoo.com', '', 1, '2016-12-31 06:24:12');

-- ----------------------------
-- Table structure for mu_pelanggan_piutang
-- ----------------------------
DROP TABLE IF EXISTS `mu_pelanggan_piutang`;
CREATE TABLE `mu_pelanggan_piutang`  (
  `id_pelanggan_piutang` int(11) NOT NULL AUTO_INCREMENT,
  `id_pelanggan` int(11) NOT NULL,
  `batas_piutang` int(11) NOT NULL,
  `batas_frekuensi` int(11) NOT NULL,
  PRIMARY KEY (`id_pelanggan_piutang`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mu_pelanggan_piutang
-- ----------------------------
INSERT INTO `mu_pelanggan_piutang` VALUES (1, 2, 750000, 5);

-- ----------------------------
-- Table structure for mu_pembelian
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian`;
CREATE TABLE `mu_pembelian`  (
  `id_pembelian` int(11) NOT NULL AUTO_INCREMENT,
  `kode_pembelian` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_kirim` date NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_type_bayar` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_pembelian` date NOT NULL,
  `tgl_terima` date NOT NULL,
  `referensi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kepada_attention` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_pembelian` datetime(0) NOT NULL,
  PRIMARY KEY (`id_pembelian`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pembelian
-- ----------------------------
INSERT INTO `mu_pembelian` VALUES (1, 'PO-000001', '2017-02-15', 1, 1, 1, 'Sudah di orderkan,..', '2017-02-13', '0000-00-00', 'Tidak ada referensi...', 'indofood@gmail.com', 1, '2017-02-13 05:36:17');
INSERT INTO `mu_pembelian` VALUES (2, 'PO-000002', '2017-02-16', 2, 4, 1, 'sudah di orderkan lagi,..', '2017-02-14', '0000-00-00', 'Tidak ada referensi...', 'karimie@gmail.com', 1, '2017-02-13 05:37:48');
INSERT INTO `mu_pembelian` VALUES (3, 'PO-000003', '2017-05-08', 1, 4, 1, 'asdasdsadas', '2017-05-09', '0000-00-00', '-', 'Direktur PT. Karimie Indonesia', 1, '2017-05-08 09:32:04');
INSERT INTO `mu_pembelian` VALUES (5, 'PO-000004', '2017-05-08', 1, 4, 1, 'Keterangan tulis disini,..', '2017-05-08', '0000-00-00', '-', 'Direktur PT. Karimie Indonesia', 1, '2017-05-08 11:01:13');
INSERT INTO `mu_pembelian` VALUES (6, 'PO-000006', '2018-10-11', 1, 1, 1, 'awdawdwadwa', '2018-10-11', '0000-00-00', 'Tambun', 'adwadwada', 1, '2018-10-11 09:22:54');
INSERT INTO `mu_pembelian` VALUES (7, 'PO-000007', '2018-10-27', 2, 4, 1, 'Pembelian Oli Shell ', '2018-10-27', '0000-00-00', 'Jaya Sakti', 'Sales Shell', 1, '2018-10-27 05:29:55');
INSERT INTO `mu_pembelian` VALUES (8, 'PO-000008', '2018-11-02', 3, 4, 1, 'awdawdaw', '2018-11-02', '0000-00-00', 'Tambun', 'adwadwada', 1, '2018-11-02 14:48:48');
INSERT INTO `mu_pembelian` VALUES (9, 'PO-000009', '2018-11-02', 2, 4, 1, 'hghhhhh', '2018-11-02', '0000-00-00', 'hhhh', 'hhhhhhhh', 1, '2018-11-02 14:52:52');
INSERT INTO `mu_pembelian` VALUES (10, 'PO-000010', '2018-11-02', 2, 4, 1, 'awdwadwa', '2018-11-02', '0000-00-00', 'wada', 'wadwadwa', 1, '2018-11-02 15:14:07');
INSERT INTO `mu_pembelian` VALUES (11, 'PO-000011', '2018-11-02', 2, 4, 1, 'asasddaw', '2018-11-02', '0000-00-00', 'dwadwad', 'wadawdwadwa', 1, '2018-11-02 15:15:43');
INSERT INTO `mu_pembelian` VALUES (12, 'PO-000012', '2018-11-02', 2, 4, 1, 'aaaaaaa', '2018-11-02', '0000-00-00', 'aaaa', 'aaaa', 1, '2018-11-02 17:30:55');

-- ----------------------------
-- Table structure for mu_pembelian_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian_detail`;
CREATE TABLE `mu_pembelian_detail`  (
  `id_pembelian_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jml_pesan` int(11) NOT NULL,
  `harga_pesan` int(11) NOT NULL,
  PRIMARY KEY (`id_pembelian_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mu_pembelian_detail
-- ----------------------------
INSERT INTO `mu_pembelian_detail` VALUES (1, 1, 2, 5, 13000);
INSERT INTO `mu_pembelian_detail` VALUES (2, 1, 3, 7, 6500);
INSERT INTO `mu_pembelian_detail` VALUES (3, 1, 1, 3, 3000);
INSERT INTO `mu_pembelian_detail` VALUES (4, 2, 1, 4, 3000);
INSERT INTO `mu_pembelian_detail` VALUES (5, 2, 3, 3, 6500);
INSERT INTO `mu_pembelian_detail` VALUES (10, 5, 2, 3, 13000);
INSERT INTO `mu_pembelian_detail` VALUES (9, 5, 1, 5, 3000);
INSERT INTO `mu_pembelian_detail` VALUES (11, 6, 4, 0, 2300);
INSERT INTO `mu_pembelian_detail` VALUES (12, 6, 2, 1, 13000);
INSERT INTO `mu_pembelian_detail` VALUES (13, 7, 2, 20, 35000);
INSERT INTO `mu_pembelian_detail` VALUES (14, 8, 5, 5, 10000);
INSERT INTO `mu_pembelian_detail` VALUES (15, 9, 5, 10, 10000);
INSERT INTO `mu_pembelian_detail` VALUES (16, 10, 5, 15, 10000);
INSERT INTO `mu_pembelian_detail` VALUES (17, 11, 6, 10, 33000);
INSERT INTO `mu_pembelian_detail` VALUES (18, 12, 7, 15, 22000);

-- ----------------------------
-- Table structure for mu_pembelian_return
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian_return`;
CREATE TABLE `mu_pembelian_return`  (
  `id_pembelian_return` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian_terima` int(11) NOT NULL,
  `no_return` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tgl_kirim_return` date NOT NULL,
  `id_type_bayar` int(11) NOT NULL,
  `keterangan_return` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_return` date NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_return` datetime(0) NOT NULL,
  PRIMARY KEY (`id_pembelian_return`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pembelian_return
-- ----------------------------
INSERT INTO `mu_pembelian_return` VALUES (1, 1, 'RET-001', '2017-02-14', 1, 'Sudah dibayarkan semua,..', '2017-02-14', 1, '2017-02-13 05:41:35');
INSERT INTO `mu_pembelian_return` VALUES (2, 1, '123', '2018-10-16', 1, '', '2018-10-29', 1, '2018-10-20 14:11:42');

-- ----------------------------
-- Table structure for mu_pembelian_return_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian_return_detail`;
CREATE TABLE `mu_pembelian_return_detail`  (
  `id_pembelian_return_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian_return` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga_return` int(11) NOT NULL,
  `jml_return` int(11) NOT NULL,
  PRIMARY KEY (`id_pembelian_return_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mu_pembelian_return_detail
-- ----------------------------
INSERT INTO `mu_pembelian_return_detail` VALUES (1, 1, 1, 3000, 0);
INSERT INTO `mu_pembelian_return_detail` VALUES (2, 1, 3, 6500, 3);
INSERT INTO `mu_pembelian_return_detail` VALUES (3, 1, 2, 13000, 1);
INSERT INTO `mu_pembelian_return_detail` VALUES (4, 2, 1, 3000, 0);
INSERT INTO `mu_pembelian_return_detail` VALUES (5, 2, 3, 6500, 0);
INSERT INTO `mu_pembelian_return_detail` VALUES (6, 2, 2, 13000, 0);

-- ----------------------------
-- Table structure for mu_pembelian_terima
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian_terima`;
CREATE TABLE `mu_pembelian_terima`  (
  `id_pembelian_terima` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian` int(11) NOT NULL,
  `no_pembelian_terima` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_surat_jalan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `pengirim_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tanggal_terima` date NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_proses` datetime(0) NOT NULL,
  PRIMARY KEY (`id_pembelian_terima`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pembelian_terima
-- ----------------------------
INSERT INTO `mu_pembelian_terima` VALUES (1, 1, 'TR-000001', 'JLN-001', 'Amaik Sapihi', 'Barang sampai dengan selamat,..', '2017-02-17', 2, 1, '2017-02-13 05:39:21');
INSERT INTO `mu_pembelian_terima` VALUES (2, 1, 'TR-000002', '', '', '', '2018-10-23', 2, 1, '2018-10-19 05:30:39');
INSERT INTO `mu_pembelian_terima` VALUES (3, 8, 'TR-000003', '', 'Jl. Raya Cakung', '', '2018-10-31', 2, 1, '2018-11-02 14:49:14');
INSERT INTO `mu_pembelian_terima` VALUES (4, 8, 'TR-000004', '12223', '1234', '12345', '2018-11-26', 2, 1, '2018-11-02 14:50:14');
INSERT INTO `mu_pembelian_terima` VALUES (5, 9, 'TR-000005', '1234', '12345', 'eeeeeeeee', '2018-11-02', 2, 1, '2018-11-02 14:53:20');
INSERT INTO `mu_pembelian_terima` VALUES (6, 10, 'TR-000006', 'awdaw', 'dwad', 'awdwadaw', '2018-11-02', 2, 1, '2018-11-02 15:14:22');
INSERT INTO `mu_pembelian_terima` VALUES (7, 11, 'TR-000007', 'wadwadwadwad', 'wadwadawdw', 'wadawdwad', '2018-11-02', 3, 1, '2018-11-02 15:15:58');
INSERT INTO `mu_pembelian_terima` VALUES (8, 12, 'TR-000008', '123', '123', '123', '2018-11-02', 2, 1, '2018-11-02 17:31:21');

-- ----------------------------
-- Table structure for mu_pembelian_terima_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_pembelian_terima_detail`;
CREATE TABLE `mu_pembelian_terima_detail`  (
  `id_pembelian_terima_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_pembelian_terima` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `harga_pembelian` int(11) NOT NULL,
  `jml_terima` int(11) NOT NULL,
  PRIMARY KEY (`id_pembelian_terima_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mu_pembelian_terima_detail
-- ----------------------------
INSERT INTO `mu_pembelian_terima_detail` VALUES (1, 1, 1, 3000, 3);
INSERT INTO `mu_pembelian_terima_detail` VALUES (2, 1, 3, 6500, 7);
INSERT INTO `mu_pembelian_terima_detail` VALUES (3, 1, 2, 13000, 5);
INSERT INTO `mu_pembelian_terima_detail` VALUES (4, 2, 1, 0, 0);
INSERT INTO `mu_pembelian_terima_detail` VALUES (5, 2, 3, 0, 0);
INSERT INTO `mu_pembelian_terima_detail` VALUES (6, 2, 2, 0, 0);
INSERT INTO `mu_pembelian_terima_detail` VALUES (7, 3, 5, 10000, 0);
INSERT INTO `mu_pembelian_terima_detail` VALUES (8, 4, 5, 10000, 5);
INSERT INTO `mu_pembelian_terima_detail` VALUES (9, 5, 5, 10000, 10);
INSERT INTO `mu_pembelian_terima_detail` VALUES (10, 6, 5, 10000, 15);
INSERT INTO `mu_pembelian_terima_detail` VALUES (11, 7, 6, 33000, 10);
INSERT INTO `mu_pembelian_terima_detail` VALUES (12, 8, 7, 22000, 15);

-- ----------------------------
-- Table structure for mu_pendapatan_list
-- ----------------------------
DROP TABLE IF EXISTS `mu_pendapatan_list`;
CREATE TABLE `mu_pendapatan_list`  (
  `id_pendapatan_list` int(11) NOT NULL AUTO_INCREMENT,
  `id_pendapatan_sub` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `jumlah_uang` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_karyawan` int(11) NOT NULL,
  `waktu_proses` datetime(0) NOT NULL,
  PRIMARY KEY (`id_pendapatan_list`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 14 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pendapatan_list
-- ----------------------------
INSERT INTO `mu_pendapatan_list` VALUES (13, 1, '2018-10-19', 900000, '', 1, '2018-10-19 18:56:36');

-- ----------------------------
-- Table structure for mu_pendapatan_main
-- ----------------------------
DROP TABLE IF EXISTS `mu_pendapatan_main`;
CREATE TABLE `mu_pendapatan_main`  (
  `id_pendapatan_main` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pendapatan_main` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_pendapatan_main`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pendapatan_main
-- ----------------------------
INSERT INTO `mu_pendapatan_main` VALUES (1, 'Penjualan');
INSERT INTO `mu_pendapatan_main` VALUES (2, 'Pendapatan Lain-lain');

-- ----------------------------
-- Table structure for mu_pendapatan_sub
-- ----------------------------
DROP TABLE IF EXISTS `mu_pendapatan_sub`;
CREATE TABLE `mu_pendapatan_sub`  (
  `id_pendapatan_sub` int(11) NOT NULL AUTO_INCREMENT,
  `id_pendapatan_main` int(11) NOT NULL,
  `nama_pendapatan_sub` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_pendapatan_sub`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pendapatan_sub
-- ----------------------------
INSERT INTO `mu_pendapatan_sub` VALUES (1, 1, 'Penjualan Barang');
INSERT INTO `mu_pendapatan_sub` VALUES (2, 2, 'Biaya Pengiriman (Ongkir)');
INSERT INTO `mu_pendapatan_sub` VALUES (3, 2, 'Diskon Biaya Pengiriman');
INSERT INTO `mu_pendapatan_sub` VALUES (4, 2, 'Potongan Pembelian');
INSERT INTO `mu_pendapatan_sub` VALUES (5, 2, 'Pendapatan Bunga');

-- ----------------------------
-- Table structure for mu_pendidikan
-- ----------------------------
DROP TABLE IF EXISTS `mu_pendidikan`;
CREATE TABLE `mu_pendidikan`  (
  `id_pendidikan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_pendidikan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_pendidikan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_pendidikan
-- ----------------------------
INSERT INTO `mu_pendidikan` VALUES (1, 'SMP', 1);
INSERT INTO `mu_pendidikan` VALUES (2, 'SMA', 1);
INSERT INTO `mu_pendidikan` VALUES (3, 'D3', 1);
INSERT INTO `mu_pendidikan` VALUES (4, 'S1', 1);

-- ----------------------------
-- Table structure for mu_penyesuaian
-- ----------------------------
DROP TABLE IF EXISTS `mu_penyesuaian`;
CREATE TABLE `mu_penyesuaian`  (
  `id_penyesuaian` int(11) NOT NULL AUTO_INCREMENT,
  `id_sebab_alasan` int(11) NOT NULL,
  `tanggal_penyesuaian` date NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `tanggal_proses` datetime(0) NOT NULL,
  PRIMARY KEY (`id_penyesuaian`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_penyesuaian
-- ----------------------------
INSERT INTO `mu_penyesuaian` VALUES (1, 2, '2017-02-13', 'Seharusnya ada tambahan lagi,..', 1, '2017-02-13 05:28:46');
INSERT INTO `mu_penyesuaian` VALUES (2, 1, '2017-02-14', 'Beberapa Barang hilang,..', 1, '2017-02-13 05:31:51');
INSERT INTO `mu_penyesuaian` VALUES (3, 1, '2018-10-13', 'adwadwa', 1, '2018-10-20 14:17:24');
INSERT INTO `mu_penyesuaian` VALUES (4, 1, '2018-10-26', 'Barang Hilang', 1, '2018-10-26 17:05:09');

-- ----------------------------
-- Table structure for mu_penyesuaian_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_penyesuaian_detail`;
CREATE TABLE `mu_penyesuaian_detail`  (
  `id_penyesuaian_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_penyesuaian` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `stok_history` int(11) NOT NULL,
  `tambah` int(11) NOT NULL,
  `kurang` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_penyesuaian_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_penyesuaian_detail
-- ----------------------------
INSERT INTO `mu_penyesuaian_detail` VALUES (1, 1, 3, 0, 5, 0, '');
INSERT INTO `mu_penyesuaian_detail` VALUES (2, 1, 2, 0, 10, 0, '');
INSERT INTO `mu_penyesuaian_detail` VALUES (3, 1, 1, 0, 15, 0, '');
INSERT INTO `mu_penyesuaian_detail` VALUES (6, 2, 1, 12, 0, 1, '');
INSERT INTO `mu_penyesuaian_detail` VALUES (7, 3, 4, 0, 4, 0, '');
INSERT INTO `mu_penyesuaian_detail` VALUES (8, 4, 1, 7, 0, 1, 'Hilang');

-- ----------------------------
-- Table structure for mu_promosi
-- ----------------------------
DROP TABLE IF EXISTS `mu_promosi`;
CREATE TABLE `mu_promosi`  (
  `id_promosi` int(11) NOT NULL AUTO_INCREMENT,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `kode_terapkan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_kategori` int(11) NULL DEFAULT 0,
  `id_subkategori` int(11) NULL DEFAULT 0,
  `beli_barang` int(11) NOT NULL,
  `jml_beli` int(11) NOT NULL,
  `bonus_barang` int(11) NOT NULL,
  `jml_bonus` int(11) NOT NULL,
  `jenis_diskon` enum('persen','uang') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `waktu_promosi` datetime(0) NOT NULL,
  PRIMARY KEY (`id_promosi`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_promosi
-- ----------------------------
INSERT INTO `mu_promosi` VALUES (1, '2017-03-31', '2017-05-05', 'barang', 0, 0, 1, 2, 2, 1, 'uang', '', 1, '2017-05-05 06:59:13');
INSERT INTO `mu_promosi` VALUES (2, '2017-03-31', '2017-04-04', 'kategori', 1, NULL, 0, 0, 0, 0, 'uang', '', 1, '2017-04-03 09:19:47');
INSERT INTO `mu_promosi` VALUES (3, '2017-03-29', '2017-04-06', 'subkategori', 0, 1, 0, 0, 0, 0, 'persen', '', 1, '2017-03-31 10:34:53');
INSERT INTO `mu_promosi` VALUES (4, '2017-03-30', '2017-04-07', 'barang', 0, 0, 3, 0, 3, 0, 'uang', '', 1, '2017-03-31 10:24:43');

-- ----------------------------
-- Table structure for mu_promosi_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_promosi_detail`;
CREATE TABLE `mu_promosi_detail`  (
  `id_promosi_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_promosi` int(11) NOT NULL,
  `id_kategori_pelanggan` int(11) NOT NULL,
  `ecer` int(11) NULL DEFAULT NULL,
  `grosir1` int(11) NULL DEFAULT NULL,
  `grosir2` int(11) NULL DEFAULT NULL,
  `grosir3` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_promosi_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 13 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of mu_promosi_detail
-- ----------------------------
INSERT INTO `mu_promosi_detail` VALUES (1, 1, 1, 150, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (2, 1, 2, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (3, 1, 3, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (4, 2, 1, 200, 300, 300, 300);
INSERT INTO `mu_promosi_detail` VALUES (5, 2, 2, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (6, 2, 3, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (7, 3, 1, 5, 10, 10, 10);
INSERT INTO `mu_promosi_detail` VALUES (8, 3, 2, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (9, 3, 3, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (10, 4, 1, 400, 500, 500, 500);
INSERT INTO `mu_promosi_detail` VALUES (11, 4, 2, 0, 0, 0, 0);
INSERT INTO `mu_promosi_detail` VALUES (12, 4, 3, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for mu_promosi_terapkan
-- ----------------------------
DROP TABLE IF EXISTS `mu_promosi_terapkan`;
CREATE TABLE `mu_promosi_terapkan`  (
  `kode_terapkan` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`kode_terapkan`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_promosi_terapkan
-- ----------------------------
INSERT INTO `mu_promosi_terapkan` VALUES ('barang', 'Barang');
INSERT INTO `mu_promosi_terapkan` VALUES ('kategori', 'Kategori');
INSERT INTO `mu_promosi_terapkan` VALUES ('subkategori', 'Subkategori');

-- ----------------------------
-- Table structure for mu_rak
-- ----------------------------
DROP TABLE IF EXISTS `mu_rak`;
CREATE TABLE `mu_rak`  (
  `id_rak` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rak` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_rak`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_rak
-- ----------------------------
INSERT INTO `mu_rak` VALUES (1, '1.1', 'Rak 1.1', 1);
INSERT INTO `mu_rak` VALUES (2, '1.2', 'Rak 1.2', 1);
INSERT INTO `mu_rak` VALUES (3, '2.1', 'Rak 2.1', 1);

-- ----------------------------
-- Table structure for mu_satuan
-- ----------------------------
DROP TABLE IF EXISTS `mu_satuan`;
CREATE TABLE `mu_satuan`  (
  `kode_satuan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`kode_satuan`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_satuan
-- ----------------------------
INSERT INTO `mu_satuan` VALUES ('kodi', 'Kodi', 1);
INSERT INTO `mu_satuan` VALUES ('bh', 'Buah', 1);
INSERT INTO `mu_satuan` VALUES ('bks', 'Bungkus', 1);
INSERT INTO `mu_satuan` VALUES ('lsn', 'Lusin', 1);

-- ----------------------------
-- Table structure for mu_sebab_alasan
-- ----------------------------
DROP TABLE IF EXISTS `mu_sebab_alasan`;
CREATE TABLE `mu_sebab_alasan`  (
  `id_sebab_alasan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sebab_alasan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_sebab_alasan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_sebab_alasan
-- ----------------------------
INSERT INTO `mu_sebab_alasan` VALUES (1, 'Hilang', 'Barang Hilang', 1);
INSERT INTO `mu_sebab_alasan` VALUES (2, 'Koreksi', 'Koreksi Barang karena kesalahan Input', 1);
INSERT INTO `mu_sebab_alasan` VALUES (3, 'Rusak', 'Barang Rusak', 1);
INSERT INTO `mu_sebab_alasan` VALUES (4, 'Saldo Awal', 'Stok Awal barang', 1);
INSERT INTO `mu_sebab_alasan` VALUES (5, 'Stok Opname', 'Selisih Stok Buku dan Stok Opname', 1);

-- ----------------------------
-- Table structure for mu_state
-- ----------------------------
DROP TABLE IF EXISTS `mu_state`;
CREATE TABLE `mu_state`  (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`state_id`) USING BTREE,
  INDEX `fk_state_country`(`country_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 36 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_state
-- ----------------------------
INSERT INTO `mu_state` VALUES (1, 1, 'Bali', 1);
INSERT INTO `mu_state` VALUES (2, 1, 'Bengkulu', 1);
INSERT INTO `mu_state` VALUES (3, 1, 'Banten', 1);
INSERT INTO `mu_state` VALUES (4, 1, 'DIY', 1);
INSERT INTO `mu_state` VALUES (5, 1, 'Gorontalo', 1);
INSERT INTO `mu_state` VALUES (6, 1, 'Irian Jaya Barat', 1);
INSERT INTO `mu_state` VALUES (7, 1, 'Jakarta', 1);
INSERT INTO `mu_state` VALUES (8, 1, 'Jambi', 1);
INSERT INTO `mu_state` VALUES (9, 1, 'Jawa Barat', 1);
INSERT INTO `mu_state` VALUES (10, 1, 'Jawa Tengah', 1);
INSERT INTO `mu_state` VALUES (11, 1, 'Jawa Timur', 1);
INSERT INTO `mu_state` VALUES (12, 1, 'Kalimantan Barat', 1);
INSERT INTO `mu_state` VALUES (13, 1, 'Kalimantan Tengah', 1);
INSERT INTO `mu_state` VALUES (14, 1, 'Kalimantan Timur', 1);
INSERT INTO `mu_state` VALUES (15, 1, 'Kalimantan Selatan', 1);
INSERT INTO `mu_state` VALUES (16, 1, 'Kepulauan Bangka Belitung', 1);
INSERT INTO `mu_state` VALUES (17, 1, 'Lampung', 1);
INSERT INTO `mu_state` VALUES (18, 1, 'Maluku', 1);
INSERT INTO `mu_state` VALUES (19, 1, 'Maluku Utara', 1);
INSERT INTO `mu_state` VALUES (20, 1, 'Nanggroe Aceh Darussalam', 1);
INSERT INTO `mu_state` VALUES (21, 1, 'Nusa Tenggara Barat', 1);
INSERT INTO `mu_state` VALUES (22, 1, 'Nusa Tenggara Timur', 1);
INSERT INTO `mu_state` VALUES (23, 1, 'Papua', 1);
INSERT INTO `mu_state` VALUES (24, 1, 'Riau', 1);
INSERT INTO `mu_state` VALUES (25, 1, 'Kepulauan Riau', 1);
INSERT INTO `mu_state` VALUES (26, 1, 'Sulawesi Barat', 1);
INSERT INTO `mu_state` VALUES (27, 1, 'Sulawesi Tengah', 1);
INSERT INTO `mu_state` VALUES (28, 1, 'Sulawesi Tenggara', 1);
INSERT INTO `mu_state` VALUES (29, 1, 'Sulawesi Selatan', 1);
INSERT INTO `mu_state` VALUES (30, 1, 'Sulawesi Utara', 1);
INSERT INTO `mu_state` VALUES (31, 1, 'Sumatra Barat', 1);
INSERT INTO `mu_state` VALUES (32, 1, 'Sumatra Selatan', 1);
INSERT INTO `mu_state` VALUES (33, 1, 'Sumatra Utara', 1);

-- ----------------------------
-- Table structure for mu_status_karyawan
-- ----------------------------
DROP TABLE IF EXISTS `mu_status_karyawan`;
CREATE TABLE `mu_status_karyawan`  (
  `id_status_karyawan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_status_karyawan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_status_karyawan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_status_karyawan
-- ----------------------------
INSERT INTO `mu_status_karyawan` VALUES (1, 'Tetap');
INSERT INTO `mu_status_karyawan` VALUES (2, 'Kontrak');

-- ----------------------------
-- Table structure for mu_status_pernikahan
-- ----------------------------
DROP TABLE IF EXISTS `mu_status_pernikahan`;
CREATE TABLE `mu_status_pernikahan`  (
  `id_status_pernikahan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_status_pernikahan` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_status_pernikahan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_status_pernikahan
-- ----------------------------
INSERT INTO `mu_status_pernikahan` VALUES (1, 'Bujang');
INSERT INTO `mu_status_pernikahan` VALUES (2, 'Menikah');
INSERT INTO `mu_status_pernikahan` VALUES (3, 'Duda');
INSERT INTO `mu_status_pernikahan` VALUES (4, 'janda');

-- ----------------------------
-- Table structure for mu_subkategori
-- ----------------------------
DROP TABLE IF EXISTS `mu_subkategori`;
CREATE TABLE `mu_subkategori`  (
  `id_subkategori` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `nama_subkategori` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_subkategori`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_subkategori
-- ----------------------------
INSERT INTO `mu_subkategori` VALUES (1, 2, 'Dingin Banget', 1);

-- ----------------------------
-- Table structure for mu_supplier
-- ----------------------------
DROP TABLE IF EXISTS `mu_supplier`;
CREATE TABLE `mu_supplier`  (
  `id_supplier` int(11) NOT NULL AUTO_INCREMENT,
  `nama_supplier` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kontak_person` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_1` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `alamat_2` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `city_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `telepon` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hp` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `website` varchar(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_pos` int(10) NOT NULL,
  `fax` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `chat` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_supplier`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_supplier
-- ----------------------------
INSERT INTO `mu_supplier` VALUES (1, 'PT. Indofood Jaya Abadi', 'Udin Badindin', 'Jln. Hangtuah No 123 Tri Lestari', 'Jln. Sudirman Said 56 Junum', 370, 31, 1, '0751659800', '081267779988', 'indofood@gmail.com', 'http://indofood.com', 45768, '0751659811', 'indofood@yahoo.com', 1, 'Supplier ini sering telat mengantarkan barang.');
INSERT INTO `mu_supplier` VALUES (4, 'PT. Karimie Indonesia', 'Tommy Utama', 'Jln. Trio Macan Raya', '', 262, 22, 1, '0751659955', '081267770011', 'karimie@gmail.com', 'http://karimie.com', 34534, '0751652323', 'karimie@yahoo.com', 1, '');
INSERT INTO `mu_supplier` VALUES (5, 'Amin', '0821313121', 'Bekasi', 'Bekasi', 59, 9, 1, '1234567', '1234567', 'amin@mail.com', 'https://mail.google.com/mail', 12345, '123456', '112345', 1, 'Amin');

-- ----------------------------
-- Table structure for mu_transaksi
-- ----------------------------
DROP TABLE IF EXISTS `mu_transaksi`;
CREATE TABLE `mu_transaksi`  (
  `id_transaksi` int(11) NOT NULL AUTO_INCREMENT,
  `kode_transaksi` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_pelanggan` int(11) NOT NULL,
  `id_type_bayar` int(11) NOT NULL,
  `id_agen_ekspedisi` int(11) NOT NULL,
  `no_resi` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `biaya_kirim` int(11) NOT NULL,
  `diskon_persen` int(11) NOT NULL,
  `diskon_rupiah` int(11) NOT NULL,
  `diskon_belanja` int(11) NOT NULL,
  `gratis_kirim` int(11) NULL DEFAULT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` enum('tunggu','proses','selesai') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'proses',
  `id_karyawan` int(11) NOT NULL,
  `waktu_proses` datetime(0) NOT NULL,
  PRIMARY KEY (`id_transaksi`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 20 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_transaksi
-- ----------------------------
INSERT INTO `mu_transaksi` VALUES (1, 'TRX-0000001', 2, 1, 0, '', 0, 0, 0, 200, 0, 7000, '', 'selesai', 1, '2017-03-18 15:36:01');
INSERT INTO `mu_transaksi` VALUES (2, 'TRX-0000002', 0, 1, 0, '', 0, 0, 0, 0, 0, 5000, '', 'selesai', 1, '2017-05-06 10:40:12');
INSERT INTO `mu_transaksi` VALUES (3, 'TRX-0000003', 0, 1, 0, '', 0, 0, 0, 0, 0, 10000, '', 'selesai', 1, '2017-05-06 10:42:56');
INSERT INTO `mu_transaksi` VALUES (10, 'TRX-0000010', 0, 1, 0, '', 0, 0, 0, 0, 0, 10000, '', 'tunggu', 1, '2017-05-15 14:44:05');
INSERT INTO `mu_transaksi` VALUES (9, 'TRX-0000004', 0, 1, 0, '', 0, 0, 0, 0, 0, 5000, '', 'selesai', 1, '2017-05-15 14:42:41');
INSERT INTO `mu_transaksi` VALUES (11, 'TRX-0000011', 0, 1, 0, '', 0, 0, 0, 0, 0, 10000, '', 'selesai', 1, '2017-05-15 14:44:46');
INSERT INTO `mu_transaksi` VALUES (12, 'TRX-0000012', 0, 1, 0, '', 0, 0, 0, 0, 0, 8000, '', 'selesai', 1, '2017-05-15 14:51:56');
INSERT INTO `mu_transaksi` VALUES (15, 'TRX-0000013', 1, 1, 0, '', 0, 0, 0, 0, 0, 200000, '', 'selesai', 1, '2017-05-15 15:01:45');
INSERT INTO `mu_transaksi` VALUES (16, 'TRX-0000016', 0, 1, 0, '', 0, 0, 0, 0, 0, 183000, '', 'selesai', 1, '2018-10-12 10:16:57');
INSERT INTO `mu_transaksi` VALUES (17, 'TRX-0000017', 0, 1, 0, '', 0, 0, 0, 0, 0, 0, '', 'selesai', 1, '2018-10-12 10:39:10');
INSERT INTO `mu_transaksi` VALUES (18, 'TRX-0000018', 0, 1, 0, 'undefined', 0, 0, 0, 0, 0, 1000000, '', 'selesai', 1, '2018-10-19 18:51:22');
INSERT INTO `mu_transaksi` VALUES (19, 'TRX-0000019', 0, 1, 0, 'undefined', 0, 0, 0, 0, 0, 200000, '', 'proses', 1, '2018-10-19 18:56:36');

-- ----------------------------
-- Table structure for mu_transaksi_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_transaksi_detail`;
CREATE TABLE `mu_transaksi_detail`  (
  `id_transaksi_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) NOT NULL,
  `id_barang` int(11) NOT NULL,
  `jumlah_jual` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kode_satuan` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `jumlah_satuan` int(11) NOT NULL,
  `diskon_jual` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `status` enum('1','0') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_transaksi_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 131 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_transaksi_detail
-- ----------------------------
INSERT INTO `mu_transaksi_detail` VALUES (109, 1, 2, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (108, 1, 1, '1', 'bh', 1, 200, 3500, '1');
INSERT INTO `mu_transaksi_detail` VALUES (110, 2, 3, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (111, 3, 3, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (112, 3, 2, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (118, 10, 1, '1', 'bh', 1, 200, 3500, '1');
INSERT INTO `mu_transaksi_detail` VALUES (117, 9, 2, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (119, 11, 2, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (120, 12, 2, '2', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (123, 15, 1, '1', 'bh', 1, 0, 180000, '1');
INSERT INTO `mu_transaksi_detail` VALUES (124, 16, 1, '1', 'bh', 1, 0, 180000, '1');
INSERT INTO `mu_transaksi_detail` VALUES (125, 16, 3, '1', 'bh', 1, 0, 2400, '1');
INSERT INTO `mu_transaksi_detail` VALUES (128, 18, 1, '5', 'bh', 1, 0, 180000, '1');

-- ----------------------------
-- Table structure for mu_transaksi_return
-- ----------------------------
DROP TABLE IF EXISTS `mu_transaksi_return`;
CREATE TABLE `mu_transaksi_return`  (
  `id_transaksi_return` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi` int(11) NOT NULL,
  `id_type_bayar` int(11) NOT NULL,
  `bayar_return` int(11) NOT NULL,
  `ket_return` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `status` enum('proses','selesai') CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'proses',
  `id_karyawan` int(11) NOT NULL,
  `waktu_return` datetime(0) NOT NULL,
  PRIMARY KEY (`id_transaksi_return`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for mu_transaksi_return_detail
-- ----------------------------
DROP TABLE IF EXISTS `mu_transaksi_return_detail`;
CREATE TABLE `mu_transaksi_return_detail`  (
  `id_transaksi_return_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_transaksi_return` int(11) NOT NULL,
  `id_transaksi_detail` int(11) NOT NULL,
  `jumlah_return` int(11) NOT NULL,
  PRIMARY KEY (`id_transaksi_return_detail`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Table structure for mu_type_bayar
-- ----------------------------
DROP TABLE IF EXISTS `mu_type_bayar`;
CREATE TABLE `mu_type_bayar`  (
  `id_type_bayar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_type_bayar` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_type_bayar`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_type_bayar
-- ----------------------------
INSERT INTO `mu_type_bayar` VALUES (1, 'Cash (Tunai)');
INSERT INTO `mu_type_bayar` VALUES (2, 'Transfer');
INSERT INTO `mu_type_bayar` VALUES (3, 'Cheque (Cek)');
INSERT INTO `mu_type_bayar` VALUES (4, 'Debet (Kartu Debit)');
INSERT INTO `mu_type_bayar` VALUES (5, 'Card (Kartu Kredit)');
INSERT INTO `mu_type_bayar` VALUES (6, 'Credit (Hutang/Kredit)');

-- ----------------------------
-- Table structure for mu_type_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `mu_type_pelanggan`;
CREATE TABLE `mu_type_pelanggan`  (
  `id_type_pelanggan` int(11) NOT NULL AUTO_INCREMENT,
  `type_pelanggan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_users` int(11) NOT NULL,
  PRIMARY KEY (`id_type_pelanggan`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of mu_type_pelanggan
-- ----------------------------
INSERT INTO `mu_type_pelanggan` VALUES (1, 'Perorangan', 1);
INSERT INTO `mu_type_pelanggan` VALUES (2, 'Badan Usaha', 1);

SET FOREIGN_KEY_CHECKS = 1;
